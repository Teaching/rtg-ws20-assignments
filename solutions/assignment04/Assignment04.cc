/// ===============================================================================================
/// Task 1.a
/// Build the line mesh
/// Draw the line mesh
///
/// Your job is to:
///     - build a line mesh for one single line
///     - draw the line mesh (i.e. drawing the VertexArray after binding it)
///
/// Notes:
///     - create an ArrayBuffer, define the attribute(s), set the data, create a VertexArray
///     - store the VertexArray to the member variable mMeshLine
///     - the primitive type is GL_LINES
///     - for the drawing, you have to set some uniforms for mShaderLine
///     - uViewMatrix and uProjectionMatrix are automatically set
///
///
void Assignment04::buildLineMesh()
{
    // Note that it is okay to not use this method at all and assemble
    // the AB in drawLine(...) every time (from the two tg::pos3 and the color)
    auto ab = ArrayBuffer::create();
    ab->defineAttribute<float>("aPosition");
    ab->bind().setData(std::vector<float>({0.0f, 1.0f}));
    mMeshLine = VertexArray::create(ab, GL_LINES);
}

void Assignment04::drawLine(tg::pos3 from, tg::pos3 to, tg::color3 color)
{
    auto shader = mShaderLine->use();
    shader.setUniform("uFrom", from);
    shader.setUniform("uTo", to);
    shader.setUniform("uColor", color);
    mMeshLine->bind().draw();
}
/// ============= STUDENT CODE END =============
/// ===============================================================================================



/// ===============================================================================================
/// Task 1.b
/// Draw the rigid body
///
/// Your job is to:
///     - compute the model matrix of the box or sphere
///     - pass it to the shader as uniform
///     - draw the appropriate VertexArray
///
/// Notes:
///     - a rigid body can consist of several shapes with one common
///       transformation matrix (rbTransform)
///     - furthermore, every shape is transformed by its own matrix (s->transform)
///       and some scaling that is defined by the radius (sphere) or halfExtent (box)
///     - the needed VertexArrays are stored in mMeshSphere and mMeshCube
///
/// ============= STUDENT CODE BEGIN =============
auto rbTransform = mRigidBody.getTransform();

for (auto const& s : mRigidBody.shapes)
{
    auto sphere = std::dynamic_pointer_cast<SphereShape>(s);
    auto box = std::dynamic_pointer_cast<BoxShape>(s);

    // draw spheres
    if (sphere)
    {
        auto model = s->transform * tg::scaling<3>(sphere->radius);
        shader.setUniform("uModelMatrix", rbTransform * model);
        mMeshSphere->bind().draw();
    }

    // draw boxes
    if (box)
    {
        auto model = s->transform * tg::scaling(box->halfExtent);
        shader.setUniform("uModelMatrix", rbTransform * model);
        mMeshCube->bind().draw();
    }
}
/// ============= STUDENT CODE END =============
/// ===============================================================================================
